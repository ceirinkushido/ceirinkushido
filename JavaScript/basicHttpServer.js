var http = require("http");
var server = http.createServer();

function processarPedido(request, response) {
    response.writeHead(200, {
        "Content-Type":
            "text/plain; charset=utf-8"
    });
    response.write("Olá Mundo!");
    response.end();
}

server.on("request", processarPedido);

server.listen(4000);