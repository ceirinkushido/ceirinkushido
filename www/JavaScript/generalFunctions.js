/*DROP DOWN MENU*/
function dropdownShow() {
    document.getElementById("idDropdown").style.display = "block";
}

function dropdownShow2() {
    document.getElementById("idDropdown2").style.display = "block";
}

function dropdownShow3() {
    document.getElementById("idDropdown3").style.display = "block";
}

function dropdownHide() {
    document.getElementById("idDropdown").style.display = "none";
}

function dropdownHide2() {
    document.getElementById("idDropdown2").style.display = "none";
}

function dropdownHide3() {
    document.getElementById("idDropdown3").style.display = "none";
}
//END OF MENU


/*IFRAME CONTROLS*/
function changeInto(x) {
    document.getElementById("myFrame").style.display = "block";
    var audio = document.getElementById("audio");
    audio.pause();
    switch (x) {
        case 1:
            // var audio = document.getElementById("audio");
            audio.play();
            document.getElementById("myFrame").src = "https://docs.google.com/spreadsheets/d/1-ejS0pDv0TbV1TjskBskBaiHAIbsP7ZVDoP1yhReXNo/edit#gid=1846685609";
            break;

        case 2:
            document.getElementById("myFrame").src = "http://localhost:8080/pages/exJs1.html";
            break;

        case 3:
            document.getElementById("myFrame").src = "http://localhost:8080/pages/Ex_Js2.html";
            break;

        case 4:
            document.getElementById("myFrame").src = "http://localhost:8080/pages/Galeria.html";
            break;

        case 5:
            document.getElementById("myFrame").src = "http://localhost:8080/pages/exForms.html";
            break;

        case 6:
            window.open('https://github.com/ceirinkushido/VSCode-TPI', '_blank');
            break;

        case 7:
            document.getElementById("myFrame").src = "http://localhost:8080/pages/aboutMe.html";
            break;

        case 8:
            document.getElementById("myFrame").src = "http://localhost:8080/pages/CTeSP_Notas.html";
            break;

        case 9:
            document.getElementById("myFrame").src = "http://localhost:8080/pages/Ex_Js3.html";
            break;

        case 10:
            document.getElementById("myFrame").src = "http://localhost:8080/pages/Ex_Js4.html";
            break;

        default:
            alert("uh uh something went wrong");
            break;
    }
}
//END OF IFRAME